
  let w = Math.floor(Math.random() * 10) + 1;
  let e = 0;
  
  function u(r) {
    e++;
    if (r == w) {
      y("Oikein!");
    } else if (e < 3) {
      if (r < w) {
        y(`Väärin. Luku on suurempi. Yrityksiä jäljellä: ${3 - e}`);
      } else if (r > w) {
        y(`Väärin. Luku on pienempi. Yrityksiä jäljellä: ${3 - e}`);
      }
    } else {
      y(`Peli loppu. Oikea luku oli ${w}`);
    }
  }
  function y(y) {
    document.getElementById("t").innerText = y;
}
